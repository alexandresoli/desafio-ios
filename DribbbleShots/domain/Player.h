//
//  Player.h
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic, strong) NSString *twitter_screen_name;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSNumber *draftees_count;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSNumber *following_count;
@property (nonatomic, strong) NSString *website_url;
@property (nonatomic, strong) NSNumber *comments_count;
@property (nonatomic, strong) NSNumber *shots_count;
@property (nonatomic, strong) NSNumber *rebounds_received_count;
@property (nonatomic, strong) NSNumber *drafted_by_player_id;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *identification;
@property (nonatomic, strong) NSNumber *rebounds_count;
@property (nonatomic, strong) NSNumber *followers_count;
@property (nonatomic, strong) NSNumber *likes_received_count;
@property (nonatomic, strong) NSNumber *likes_count;
@property (nonatomic, strong) NSNumber *comments_received_count;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *avatar_url;


- (id)initWithDictionary:(NSDictionary *)dic;

@end

//
//  Shot.h
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface Shot : NSObject

@property (nonatomic, strong) NSNumber *identification;
@property (nonatomic, strong) NSString *descrip;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *image_teaser_url;
@property (nonatomic, strong) NSString *rebound_source_id;
@property (nonatomic, strong) NSNumber *views_count;
@property (nonatomic, strong) NSNumber *width;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) NSString *created_at;
@property (nonatomic, strong) NSNumber *rebounds_count;
@property (nonatomic, strong) NSString *image_400_url;
@property (nonatomic, strong) NSNumber *height;
@property (nonatomic, strong) Player *player;
@property (nonatomic, strong) NSNumber *likes_count;
@property (nonatomic, strong) NSNumber *comments_count;
@property (nonatomic, strong) NSString *short_url;

- (id)initWithDictionary:(NSDictionary *)dic;

@end

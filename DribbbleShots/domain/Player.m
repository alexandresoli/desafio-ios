//
//  Player.m
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import "Player.h"

@implementation Player


- (id)initWithDictionary:(NSDictionary *)dic
{
    
    self = [super init];
    
    if (self) {
        
        self.twitter_screen_name = [dic objectForKey:@"twitter_screen_name"];;
        self.location = [dic objectForKey:@"location"];;
        self.draftees_count = [dic objectForKey:@"draftees_count"];;
        self.url = [dic objectForKey:@"following_count"];
        self.website_url = [dic objectForKey:@"website_url"];
        self.comments_count = [dic objectForKey:@"comments_count"];
        self.shots_count = [dic objectForKey:@"shots_count"];
        self.rebounds_received_count = [dic objectForKey:@"rebounds_received_count"];
        self.drafted_by_player_id = [dic objectForKey:@"drafted_by_player_id"];
        self.name = [dic objectForKey:@"name"];
        self.rebounds_count = [dic objectForKey:@"rebounds_count"];
        self.followers_count = [dic objectForKey:@"followers_count"];
        self.likes_received_count = [dic objectForKey:@"likes_received_count"];
        self.likes_count = [dic objectForKey:@"likes_count"];
        self.comments_received_count = [dic objectForKey:@"comments_received_count"];
        self.username = [dic objectForKey:@"username"];
        self.avatar_url = [dic objectForKey:@"avatar_url"];
        
        
    }
    
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@\n%@",
            self.twitter_screen_name,
            self.location,
            self.draftees_count,
            self.url,
            self.website_url,
            self.comments_count,
            self.shots_count,
            self.rebounds_received_count,
            self.drafted_by_player_id,
            self.name,
            self.rebounds_count,
            self.followers_count,
            self.likes_received_count,
            self.likes_count,
            self.comments_received_count,
            self.username,
            self.avatar_url];
}



@end

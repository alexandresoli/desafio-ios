//
//  Shot.m
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import "Shot.h"

@implementation Shot

+ (NSDateFormatter *)dateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy/MM/dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}

- (id)initWithDictionary:(NSDictionary *)dic
{

    self = [super init];
    
    if (self) {
        self.identification = [dic objectForKey:@"id"];;
        self.descrip = [dic objectForKey:@"description"];;
        self.image_teaser_url = [dic objectForKey:@"image_teaser_url"];;
        self.image_url = [dic objectForKey:@"image_url"];
        self.views_count = [dic objectForKey:@"views_count"];
        self.title = [dic objectForKey:@"title"];
        
        NSDictionary *playerDic = [dic objectForKey:@"player"];
        self.player = [[Player alloc] initWithDictionary:playerDic];
    }
    
    return self;
}
- (NSString *)description {
    return [NSString stringWithFormat:@"%@\n%@\n%@\n%@", self.identification, self.descrip,self.image_teaser_url,self.image_url];
}


@end

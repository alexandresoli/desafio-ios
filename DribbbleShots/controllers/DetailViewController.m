//
//  DetailViewController.m
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import "DetailViewController.h"
#import "Shot.h"
#import "Player.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DetailViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *gTitle;
@property (nonatomic, weak) IBOutlet UILabel *counter;
@property (nonatomic, weak) IBOutlet UILabel *name;
@property (nonatomic, weak) IBOutlet UIWebView *webview;
@property (nonatomic, weak) IBOutlet UIImageView *photo;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.gTitle.text.length > 20) {
        self.gTitle.text = [_selectedShot.title substringFromIndex:20];
    } else {
        self.gTitle.text = _selectedShot.title;
    }
    
    self.gTitle.text = _selectedShot.title;
    self.counter.text = _selectedShot.views_count.stringValue;
    self.name.text = _selectedShot.player.name;
    
    // sanity check
    if (_selectedShot.descrip != (id)[NSNull null]) {
        [self.webview loadHTMLString:_selectedShot.descrip baseURL:nil];
        [self.webview stringByEvaluatingJavaScriptFromString: @"document.body.style.fontFamily = 'Helvetica-Neue'"];
        
    }
    
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:_selectedShot.image_url] placeholderImage:[UIImage imageNamed:@"downloading_icon.png"]];
    [self.photo sd_setImageWithURL:[NSURL URLWithString:_selectedShot.player.avatar_url] placeholderImage:[UIImage imageNamed:@"downloading_icon.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Close
-(IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}


#pragma mark - UIWebViewDelegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked) {
        
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
        
    }
    
    return YES;
}

#pragma mark - Social Sharing
- (IBAction)facebookShare:(id)sender
{
    [self showMessage:@"Compartilhar no Facebook?"];
}

- (IBAction)twitterShare:(id)sender
{
    [self showMessage:@"Compartilhar no Twitter?"];
}

- (IBAction)pinterestShare:(id)sender
{
    [self showMessage:@"Compartilhar no Pinterest?"];
}

- (IBAction)instagramShare:(id)sender
{
    [self showMessage:@"Compartilhar no Instagram?"];
}

- (void)showMessage:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:message
                                          preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];

    
    
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];

    [self presentViewController:alertController animated:YES completion:nil];
}
@end

//
//  DetailViewController.h
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Shot;

@interface DetailViewController : UIViewController

@property (nonatomic, strong) Shot *selectedShot;
@property (nonatomic, strong) UIImage *selectedImage;

@end

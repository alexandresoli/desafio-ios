//
//  PopularTableViewController.m
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import "PopularTableViewController.h"
#import "AFHTTPRequestOperationManager.h"
//#import "UIImageView+AFNetworking.h"
#import "ShotTableViewCell.h"
#import "Shot.h"
#import "DetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PopularTableViewController ()

@property (nonatomic, strong) NSMutableArray *shots;
@property (nonatomic, strong) Shot *selectedShot;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic) int lastVisited;
@end

@implementation PopularTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // http://stackoverflow.com/questions/18900428/ios-7-uitableview-shows-under-status-bar
    self.tableView.contentInset = UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f);
    
    self.shots = [NSMutableArray array];
    
    [self.tableView setAccessibilityIdentifier:@"tableview"];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self fetchShots];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.shots.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CellIdentifier";
    
    ShotTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Shot *shot = [_shots objectAtIndex:indexPath.section];
    
    
    if (shot.title.length > 20) {
        cell.title.text = [shot.title substringFromIndex:20];
    } else {
        cell.title.text = shot.title;
    }
    
    cell.viewCount.text = shot.views_count.stringValue;
    
    // utilizando categoria do AFNetworking para download e cache assincrono de imagens
    
    if (shot.image_teaser_url != (id)[NSNull null]) {
        [cell.imgView sd_setImageWithURL:[NSURL URLWithString:shot.image_url] placeholderImage:[UIImage imageNamed:@"downloading_icon.png"]];
    }
    
    // paginação
    if (indexPath.section == self.shots.count - 1)
    {
        [self fetchShots];
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedShot = [self.shots objectAtIndex:indexPath.section];
    
    ShotTableViewCell *cell = (ShotTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    self.selectedImage = cell.imageView.image;
    
    [self performSegueWithIdentifier:@"DetailSegue" sender:nil];
}

#pragma mark - Popular Shots Fetch
- (void)fetchShots
{
    _lastVisited++;
    
    NSString *urlString = [NSString stringWithFormat:@"http://api.dribbble.com/shots/popular?access_token=%@&page=%d",DRIBBLE_ACCESS_TOKEN,_lastVisited];
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *shotsDic = [responseObject objectForKey:@"shots"];
        
        for (NSDictionary *dictionary in shotsDic) {
            Shot *shot = [[Shot alloc] initWithDictionary:dictionary];
            [_shots addObject:shot];
        }
        
        [self.tableView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed: %@",error.localizedDescription);
    }];
    
    [operation start];
    
}

#pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    DetailViewController *controller = segue.destinationViewController;
    controller.selectedShot = self.selectedShot;
    controller.selectedImage = self.selectedImage;
    
}

@end

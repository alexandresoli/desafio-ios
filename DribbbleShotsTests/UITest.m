//
//  UITest.m
//  DribbbleShots
//
//  Created by Alexandre Oliveira on 3/5/15.
//  Copyright (c) 2015 Alexandre Oliveira. All rights reserved.
//

#import "UITest.h"

@implementation UITest


- (void)beforeEach
{
    NSLog(@"starting");
}

- (void)afterEach
{
    NSLog(@"ending");
}

- (void)testSuccessfulLogin
{
    [tester scrollViewWithAccessibilityIdentifier:@"tableview" byFractionOfSizeHorizontal:2.0 vertical:2.0];
}

@end
